# number
variable "number_example" {
  description   = "An example of a number variable in Terraform"
  type          = number
  default       = 42
}

# list
variable "list_example" {
  description   = "An example of a list in Terraform"
  type          = list
  default       = ["a", "b", "c"]
}

# combination of list and number
variable "list_numeric_example" {
  description   = "An example of a numeric list in Terraform"
  type          = list(number)
  default       = [1, 2, 3]
}

# map
variable "map_example" {
  description   = "An example of a map in Terraform"
  type          = map(string)
  
  default       = {
    key1    = "Value1"
    key2    = "Value2"
    key3    = "Value3"
  }
}

# create a complicated data structure using object and tuple
variable "object_example" {
  description   = "An example of a structural type in Terraform"
  type          = object({
    name    = string
    age     = number
    tags    = list(string)
    enabled = bool
  })
  
  default   = {
    name    = "value1"
    age     = 42
    tags    = ["a", "b", "c"]
    enabled = true
  }
}

# port
variable "server_port" {
  description   = "The port the server will use for HTTP requests"
  type          = number
  default       = 8080
}

# output public ip once deployment has been made
output "alb_dns_name" {
  value       = aws_instance.example.public_ip
  description = "The domain name of the loader balancer"
}
