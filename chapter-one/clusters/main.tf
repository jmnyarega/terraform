provider "aws" {
  region      = "us-east-1"
  profile     = "default"
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}

variable "elb_port" {
  description   = "Load Balancer Port"
  type          = number
  default       = 80
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group" "clusters" {
  name = "clusters"

  ingress {
    from_port     = var.server_port
    to_port       = var.server_port
    protocol      = "tcp"
    cidr_blocks   = ["0.0.0.0/0"]
  }
}

resource "aws_launch_configuration" "clusters" {
  image_id          = "ami-02eac2c0129f6376b"
  instance_type     = "t2.micro"
  security_groups   = [aws_security_group.clusters.id]
  user_data         = <<-EOF
                      #!/bin/bash
                      echo "Hello, World" > index.html
                      nohup busybox httpd -f -p ${var.server_port} &
                      EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "clusters" {
  launch_configuration = aws_launch_configuration.clusters.name
  vpc_zone_identifier  = data.aws_subnet_ids.default.ids 
  
  target_group_arns    = [aws_lb_target_group.asg.arn]
  health_check_type    = "ELB"

  min_size             = 3
  max_size             = 10
  tag {
    key                 = "Name"
    value               = "terraform-asg-clusters"
    propagate_at_launch = true
  }
}

resource "aws_lb" "clusters" {
  name                = "terraform-asg-clusters"
  load_balancer_type  = "application"
  subnets             = data.aws_subnet_ids.default.ids
  security_groups     = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn   = aws_lb.clusters.arn
  port                = var.elb_port
  protocol            = "HTTP"
  
  # by default, return a simple 404 page
  default_action {
    type    = "fixed-response"

    fixed_response {
      content_type    = "text/plain"
      message_body    = "404: page not found"
      status_code     = 404
    }
  }
}

resource "aws_security_group" "alb" {
  name  = "terraform-clusters-alb"
  
  # allow all outbound HTTP requests
  ingress {
    from_port   = var.elb_port
    to_port     = var.elb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks =  ["0.0.0.0/0"]
  }
}

resource "aws_lb_target_group" "asg" {
  name      = "terrform-asg-clusters"
  port      = var.server_port
  protocol  = "HTTP"
  vpc_id    = data.aws_vpc.default.id
  
  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn  = aws_lb_listener.http.arn
  priority      = 100
  
  condition {
    field   = "path-pattern"
    values  = ["*"]
  }

  action {
    type                = "forward"
    target_group_arn    = aws_lb_target_group.asg.arn
  }
}

output "alb_dns_name" {
  value       = aws_lb.clusters.dns_name
  description = "The domain name of the load balancer"
}
