provider "aws" {
  region      = "us-east-1"
  profile     = "default"
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}

resource "aws_instance" "web" {
  ami                     = "ami-08b314ce48a790a19"
  instance_type           = "t2.micro"
  vpc_security_group_ids  = [aws_security_group.web.id]
  
  user_data = <<-EOF
              #!/bin/bash
              echo "Hello, World" > index.html
              nohup busybox httpd -f -p ${var.server_port} &
              EOF

  tags = {
    Name = "web-instance"
  }
}

resource "aws_security_group" "web" {
  name = "terraform-web-instance"
  
  ingress {
    from_port   = var.server_port
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "public_ip" {
  value         =  aws_instance.web.public_ip
  description   = "The public IP address of the web server"
}
